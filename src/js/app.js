var awards = {
  awardsItem: null,
  awardsItemActive: null,

  init: function () {
    var _this = this;

    this.selectYears = '.select--custom[name="awards-year"]';
    this.selectModels = '.select--custom[name="awards-model"]';
    this.selectTypeAwards = '.select--custom[name="awards-item"]';

    this.pageUrl = location.pathname;

    // контейнер для наград
    this.$awardsLayout = $('.awards-layout');

    // получаем награды из API
    _this.getAwards();

    // событие change на селекты фильтра
    _this.changeYear();
    _this.changeModel();
    _this.changeTypeAward();
  },

  getAwards: function () {
    var _this = this;

    $.ajax({
      url: '/static/js/awards.json',
      success: function (data) {
        _this.dataYear = data.year;
        _this.dataModel = data.model;
        _this.dataTypeAward = data.type_award;
        _this.dataAwards = data.awards;

        _this.fillSelectYears(_this.dataYear);
        _this.fillSelectModels(_this.dataModel);
        _this.fillSelectTypeAwards(_this.dataTypeAward);

        // page load
        _this.pageLoadAwards();
      },
      error: function () {
        console.log('error');
      }
    });
  },

  fillSelectYears: function (data) {
    var _this = this;

    // заполняем селект данными
    _this.fillSelect(data, _this.selectYears, _this.year);
  },

  changeYear: function () {
    var _this = this;

    $(_this.selectYears).on('change', function () {
      _this.year = $(this).val();

      _this.fillSelectModels(_this.dataYear[_this.year].model);
      _this.fillSelectTypeAwards(_this.dataYear[_this.year].type_award);

      _this.filterAwards();

      _this.scrollToBlock($('.awards-filter').offset().top);
    });
  },

  fillSelectModels: function (data) {
    var _this = this;

    // заполняем селект данными
    _this.fillSelect(data, _this.selectModels, _this.model);
  },

  changeModel: function () {
    var _this = this;

    $(_this.selectModels).on('change', function () {
      _this.model = $(this).val();

      _this.fillSelectYears(_this.dataModel[_this.model].year);
      _this.fillSelectTypeAwards(_this.dataModel[_this.model].type_award);

      _this.filterAwards();

      _this.scrollToBlock($('.awards-filter').offset().top);
    });
  },

  fillSelectTypeAwards: function (data) {
    var _this = this;

    // заполняем селект данными
    _this.fillSelect(data, _this.selectTypeAwards, _this.typeAward);
  },

  changeTypeAward: function () {
    var _this = this;

    $(_this.selectTypeAwards).on('change', function () {
      _this.typeAward = $(this).val();

      _this.fillSelectYears(_this.dataTypeAward[_this.typeAward].year);
      _this.fillSelectModels(_this.dataTypeAward[_this.typeAward].model);

      _this.filterAwards();

      _this.scrollToBlock($('.awards-filter').offset().top);
    });
  },

  fillSelect: function (data, select, change) {
    var _this = this,
      option;

    $(select).empty();

    if (!data.length) {
      for (var item in data) {
        option = '<option value="' + item + '">' + item + '</option>';
        $(select).append(option);
      }
    } else {
      for (var i = 0; i < data.length; i++) {
        option = '<option value="' + data[i] + '">' + data[i] + '</option>';
        $(select).append(option);
      }
    }

    if (change && $(select + ' option[value="' + change + '"]').length > 0) {
      $(select + ' option[value="' + change + '"]').prop('selected', true);
    } else {
      switch (change){
        case _this.year:
          _this.year = 'Все года';
          break;
        case _this.model:
          _this.model = 'Все модели';
          break;
        case _this.typeAward:
          _this.typeAward = 'Все награды';
          break;
      }
    }
  },

  fillAwards: function () {
    _.templateSettings.variable = 'rc';

    var _this = this,
      template = _.template($('script#template-awards').html()),
      data = {};

    data['awards'] = _this.dataAwards;

    for (year in _this.dataYear) {
     if (year == 'Все года') {
       continue;
     }

      data['year'] = year;

     $('.awards').prepend('' +
        '<div class="awards__year">' +
        '<div class="awards__year__title">'+ year +'</div>' +
        '<div class="awards-layout">' +
          template(data) +
        '</div>' +
        '</div>');
    }
  },

  filterAwards: function () {
    var _this = this,
      awardsItem = $('.awards-item'),
      item;

    // закрываем все активные награды
    _this.closeItemAward(true);

    if (
      _this.year == 'Все года'
      || _this.model == 'Все модели'
      || _this.typeAward == 'Все награды'
    ) {
      awardsItem.fadeIn(0).removeClass('fade-out');
    }

    awardsItem.each(function () {
      item = $(this);

      if (
        (_this.year != 'Все года' && item.attr('data-year') != _this.year)
        || (_this.model != 'Все модели' && item.attr('data-model') != _this.model)
        || (_this.typeAward != 'Все награды' && item.attr('data-type') != _this.typeAward)
      ) {
        item.fadeOut(0).addClass('fade-out');
      } if (
        (_this.year != 'Все года' && item.attr('data-year') == _this.year)
        && (_this.model != 'Все модели' && item.attr('data-model') == _this.model)
        && (_this.typeAward != 'Все награды' && item.attr('data-type') == _this.typeAward)
      ) {
        item.fadeIn(0).removeClass('fade-out');
      } else {
        return;
      }
    });

    if (awardsItem.not('.fade-out').length == 0) {
      _this.year = 'Все года';
      _this.model = 'Все модели';
      _this.typeAward = 'Все награды';
      $(_this.selectYears).val(_this.year);
      $(_this.selectModels).val(_this.model);
      $(_this.selectTypeAwards).val(_this.typeAward);
      _this.filterAwards();
    }

    // скрываем \ показываем контейнер с годом
    $('.awards-layout').each(function () {
      if ($(this).find('.awards-item').not('.fade-out').length > 0) {
        $(this).closest('.awards__year').show();
      } else {
        $(this).closest('.awards__year').hide();
      }
    });
  },

  openItemAward: function () {
    var _this = this;

    _this.awardsItemActive.addClass('active');
    _this.awardsItemId = _this.awardsItemActive.attr('data-id');

    // устанавливаем url страницы
    history.pushState('','', '?year=' + _this.year + '&id=' + _this.awardsItemId);

    // scroll to block
    _this.scrollToBlock(_this.awardsItemActive.offset().top);
  },

  closeItemAward: function (scroll) {
    var _this = this;

    if (!_this.awardsItemActive){
      return;
    }

    _this.awardsItemActive.removeClass('active');

    if (!scroll) {
      // scroll to block
      _this.scrollToBlock(_this.awardsItemActive.offset().top);
    }

    // устанавливаем url страницы
    history.pushState('','', _this.pageUrl);
  },

  scrollToBlock: function (block) {
    var scrollTop,
      heightNavPrimary = ($('#nav-primary'))? $('#nav-primary').height(): 0;

    scrollTop = block - 30 - heightNavPrimary;
    $('html, body').animate({scrollTop: scrollTop}, 500);
  },

  pageLoadAwards: function () {
    var _this = this;

    // page load data
    _this.year = getParamsUrl('year') || '';
    _this.awardsItemId = getParamsUrl('id') || '';

    if (!_this.year){
      _this.year = '2016';
    } else {
      _this.year = decodeURIComponent(_this.year);
    }

    _this.model = $(_this.selectModels).val();
    _this.typeAward = $(_this.selectTypeAwards).val();

    // устанавливаем значение select[year]
    // и предзаполняем select[models] и select[type]
    $(_this.selectYears).val(_this.year);
    _this.fillSelectModels(_this.dataYear[_this.year].model);
    _this.fillSelectTypeAwards(_this.dataYear[_this.year].type_award);

    // выводим награды
    _this.fillAwards();
    // фильтр наград
    _this.filterAwards();

    // открываем карточку награды
    if (_this.awardsItemId) {
      _this.awardsItemActive = $('.awards-item[data-id="' + _this.awardsItemId + '"]');
      _this.openItemAward();
    }

    // открыть / закрыть награду
    $('.js-awards-item__btn--open').on('click', function () {
      _this.closeItemAward(true);
      _this.awardsItemActive = $(this).closest('.awards-item');
      _this.openItemAward();
    });

    $('.js-awards-item__btn--close').on('click', function () {
      _this.closeItemAward();
    });
  }
};

//получаем конкретный get параметр
function getParamsUrl(key) {
  var s = window.location.search;
  s = s.match(new RegExp(key + '=([^&=]+)'));
  return s ? s[1] : false;
}

//yandex share
function socialShare(idShare){
  var url = location.href,
    $share = $('#' + idShare),
    title = $share.siblings('.js-share-title').text(),
    description = $share.siblings('.js-share-desc').text(),
    image = $share.parents('.acsrs-item').find('.js-share-img').attr('src');

  var share = Ya.share2(idShare, {
    content: {
      url: url,
      title: title,
      description: description,
      image: image
    },

    theme: {
      services: 'vkontakte,facebook,twitter,odnoklassniki',
      counter: false,
      lang: 'ru',
      size: 'm',
      bare: true
    },

    hooks: {
      onready: function () {
        //alert('блок инициализирован');
      },

      onshare: function (name) {
        //alert('нажата кнопка' + name);
      }
    }

    //contentByService: {
    //  twitter: {
    //    url: 'https://ya.ru',
    //    title: 'Яндекс',
    //    description: 'Яндекс — добро.'
    //  }
    //}
  });
}

var up = {
  nodeContainer : null,

  init : function(params){
    var _this = this;

    this.nodeContainer = params.nodeContainer;

    $('body').append('<div class=' + this.nodeContainer + '></div>');

    _this.checkRange();

    $(window).scroll(function(){
      _this.checkRange();
    });

    _this.clickUp();
  },

  clickUp : function(){
    var _this = this;

    $('.' + _this.nodeContainer).on('click', function(){
      $('body, html').animate({'scrollTop' : 0}, 500);
    });
  },

  checkRange : function(){
    var _this = this,
      positionScroll = $(window).scrollTop();

    if(positionScroll >= 200){
      $('.' + _this.nodeContainer).fadeIn();
      return true;
    } else {
      $('.' + _this.nodeContainer).fadeOut();
      return false;
    }
  }
};


//document.ready
$(document).ready(function(){
  //accessories.init();

  //up.init({nodeContainer: 'upupup'});
});