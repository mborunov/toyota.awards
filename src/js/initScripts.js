var headTag = document.head || document.getElementsByTagName('head')[0];

var siteDomain = ''; // localhost
//var siteDomain = '//toyota-awards.dev.bstd.ru'; // разработка dev
//var siteDomain = '//dreamtown.toyota.ru'; // production

var arrayCss = [
  siteDomain + '/static/js/lib/animate.css/animate.min.css',
  siteDomain + '/static/css/app.css'
];

for(var c = 0; c < arrayCss.length; c++) {
  var link = document.createElement('link');

  link.setAttribute('href', arrayCss[c]);
  link.setAttribute('rel', 'stylesheet');

  headTag.appendChild(link);
}

var arrayJs = [
  siteDomain + '/static/js/lib/underscore/underscore-min.js',
  siteDomain + '/static/js/app.js'
];

for(var j = 0; j < arrayJs.length; j++) {
  var script = document.createElement('script');

  script.setAttribute('src', arrayJs[j]);

  headTag.appendChild(script);
}

var pageLoad = {
  init : function(){
    var _this = this;

    $.ajax({
      url : siteDomain + '/page.html',
      dataType : 'html',
      success: function(data){
        $('#minisiteContent').html(data);
        _this.initScripts();
      }
    });

  },

  initScripts : function(){
    if(typeof window['awards'] == 'undefined'){
      setTimeout(pageLoad.initScripts, 50);
      return true;
    }

    setTimeout(function () {
      awards.init();
      up.init({nodeContainer: 'upupup'});
    }, 1000);
  }
};


$(document).ready(function () {
  pageLoad.init();
});